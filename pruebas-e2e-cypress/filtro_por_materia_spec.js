describe('Filtro por materia', function() {
    it('Filtro por materia en página de profesor', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        //Lineas nuevas  
        cy.get('.buscador').find('input').type('Mario Linares Vasquez', { force: true})
        cy.contains('Mario Linares Vasquez - Ingeniería de Sistemas').click()
        cy.wait(2000)
        cy.get('.materias').find('input[name="id:MISO4208"]').click()
    })
})