import {browser, by, element, ElementFinder} from 'protractor';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  getDetailHero(){
    return element.all(by.tagName('h2')).getText();
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  searchHeroInInput(searchHero: string){
    element(by.css('#search-box')).sendKeys(searchHero);
    element(by.css('.search-result')).click();
  }

  deleteHero(idHero: number){
    element(by.css('.heroes')).all(by.css('.delete')).get(idHero).click();
  }

  editHero(updateHero: string){
    element(by.tagName('input')).clear();
    element(by.tagName('input')).sendKeys(updateHero);
    element(by.buttonText('Save')).click();
  }

  navigateHeroFromDashboard(idHero: number){
    element.all(by.css('.module.hero')).get(idHero).click();
  }

  navigateHeroFromListHeroes(idHero: number){
    element.all(by.css('.badge')).get(idHero).click();
    element(by.buttonText('View Details')).click();
  }
}
