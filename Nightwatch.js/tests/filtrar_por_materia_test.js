module.exports = { // adapted from: https://git.io/vodU0
    'Los estudiantes ir a pagina profesor': function(browser) {
        browser
            .url('https://losestudiantes.co/')
            .click('.botonCerrar')
            .waitForElementVisible('.buscador', 5000)
            .click('.buscador')
            .setValue('.buscador input[role="combobox"]', 'Mario Linares')
            .waitForElementVisible('.Select-option', 5000)
            .assert.containsText('.Select-option', 'Mario Linares Vasquez - Ingeniería De Sistemas')
            .click('.Select-option')
            .waitForElementVisible('.nombreProfesor', 5000)
            .assert.containsText('.nombreProfesor', 'Mario Linares Vasquez')
            .waitForElementVisible('.materias', 5000)
            .click('input[name="id:MISO4208"]')
            .waitForElementVisible('#escribir-resena', 15000)
            .setValue('#escribir-resena', 'Reseña para profesor')
            .end();
    }
};