var assert = require('assert');
describe('los estudiantes login', function() {

    beforeEach(function() {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 40000;
    });

    afterEach(function() {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaSignUp = browser.element('.cajaSignUp');

        var nombreInput = cajaSignUp.element('input[name="nombre"]');
        nombreInput.click();
        nombreInput.keys('Fabian');

        var apellidoInput = cajaSignUp.element('input[name="apellido"]').click();
        apellidoInput.keys('Lopez');

        var correoInput = cajaSignUp.element('input[name="correo"]').click();
        correoInput.keys('fabiantriana1072@gmail.com');

        var selectBox = cajaSignUp.element('select[name="idPrograma"]');
        selectBox.selectByVisibleText('Ingeniería Civil');

        var passwordInput = cajaSignUp.element('input[name="password"]').click();
        passwordInput.keys('123456789');
        cajaSignUp.element('input[name="acepta"]').click();

        browser.waitForVisible('button=Registrarse', 10000);
        browser.click('button=Registrarse');

        browser.waitForVisible('.text-muted.lead', 10000);
        var alertText = browser.element('.text-muted.lead').getText();
        expect(alertText).toBe("Error: Ya existe un usuario registrado con el correo 'fabiantriana1072@gmail.com'");


    });
});

